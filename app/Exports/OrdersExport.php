<?php

namespace App\Exports;

use App\Orders;
use App\OrderDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\Helper;

class OrdersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection_tmp()
    {
        return Orders::all();
    }
    public function collection()
    {
        $orders = Orders::all();
        foreach ($orders as $row) {
            if($row->type==2){
                if($row->status==1 && $row->status_delivery==0) {
                    //moi 0
                    $row->status = 0;
                }
                else if($row->status==1 && $row->status_delivery==1) {
                    //da giao 1
                    $row->status = 1;
                }
                else if($row->status==1 && $row->status_delivery==-1) {
                    //huy 1
                    $row->status = -1;
                }else{
                    $row->status= $row->status;
                }
            }else{
                $row->status= $row->status;
            }
            $order[] = array(
                '0' => $row->id,
                '1' => $row->code,
                '2' => $row->address_buyer,
                '3' => $row->phone_buyer,
                '4' => $row->created_at,
                '5' => number_format($row->total),
                '6' => Helper::statusDelivery($row->status)
            );
            $order_detail = OrderDetail::where("order_id",$row->id)->get();
            foreach ($order_detail as $row2) {
                $order[] = array(
                    '0' => '',
                    '1' => 'Tên sp: '.$row2->product_name,
                    '2' => 'Số lượng: '.$row2->amount,
                    '3' => 'Đơn giá: '.number_format($row2->price),
                    '4' => '',
                    '5' => '',
                    '6' => ''
                );
            }

        }

        return (collect($order));
    }
    public function headings(): array
    {
        return [
            'id',
            'Đơn hàng',
            'Địa chỉ GH',
            'Điện thoại GH',
            'Ngày đặt hàng',
            'Tổng tiền',
            'Trạng thái',
        ];
    }
}
