<?php

namespace App\Exports;

use App\Users;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\Helper;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = Users::all();
        foreach ($users as $row) {
            
            $user[] = array(
                '1' => $row->name,
                '2' => $row->email,
                '3' => $row->phone,
                '4' => $row->address,
                '5' => $row->birthday,
                '6' => Helper::gender($row->gender),
                '7' => Helper::typeBrand($row->type_brand),
            );
        }

        return (collect($user));
    }

    public function headings(): array
    {
        return [
            'Họ tên',
            'Email',
            'Điện thoại',
            'Địa chỉ',
            'Ngày sinh',
            'Giới tính',
            'Nhà thuốc- cá nhân'
        ];
    }
}
