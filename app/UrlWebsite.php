<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlWebsite extends Model
{
    protected $table= "url_internal_website";
    protected $hidden = ["created_at", "updated_at"];
}
