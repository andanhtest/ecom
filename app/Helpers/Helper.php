<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Hash;
class Helper {

    public static function generate_token() {
        //return time() * 3600;  // 30 days
        return time() +20;  // 30 days
    }
    public static function checkExistLogin(){
        if(Auth::guard('admin')->check()){            
            $admin = Auth::guard('admin')->user(); 
            view()->share('admin_login',$admin);           
        }
    }
    public static function convert_alias($str)
    {
        global $VMAP;
        $VMAP = array(
            'ế' => 'e',
            'ề' => 'e',
            'ể' => 'e',
            'ễ' => 'e',
            'ệ' => 'e',
            #---------------------------------E^
            'Ế' => 'e',
            'Ề' => 'e',
            'Ể' => 'e',
            'Ễ' => 'e',
            'Ệ' => 'e',
            #---------------------------------e
            'é' => 'e',
            'è' => 'e',
            'ẻ' => 'e',
            'ẽ' => 'e',
            'ẹ' => 'e',
            'ê' => 'e',
            #---------------------------------E
            'É' => 'e',
            'È' => 'e',
            'Ẻ' => 'e',
            'Ẽ' => 'e',
            'Ẹ' => 'e',
            'Ê' => 'e',
            #---------------------------------i
            'í' => 'i',
            'ì' => 'i',
            'ỉ' => 'i',
            'ĩ' => 'i',
            'ị' => 'i',
            #---------------------------------I
            'Í' => 'i',
            'Ì' => 'i',
            'Ỉ' => 'i',
            'Ĩ' => 'i',
            'Ị' => 'i',
            #---------------------------------o^
            'ố' => 'o',
            'ồ' => 'o',
            'ổ' => 'o',
            'ỗ' => 'o',
            'ộ' => 'o',
            #---------------------------------O^
            'Ố' => 'o',
            'Ồ' => 'o',
            'Ổ' => 'o',
            'Ô' => 'o',
            'Ộ' => 'o',
            #---------------------------------o*
            'ớ' => 'o',
            'ờ' => 'o',
            'ở' => 'o',
            'ỡ' => 'o',
            'ợ' => 'o',
            #---------------------------------O*
            'Ớ' => 'o',
            'Ờ' => 'o',
            'Ở' => 'o',
            'Ỡ' => 'o',
            'Ợ' => 'o',
            #---------------------------------u*
            'ứ' => 'u',
            'ừ' => 'u',
            'ử' => 'u',
            'ữ' => 'u',
            'ự' => 'u',
            #---------------------------------U*
            'Ứ' => 'u',
            'Ừ' => 'u',
            'Ử' => 'u',
            'Ữ' => 'u',
            'Ự' => 'u',
            #---------------------------------y
            'ý' => 'y',
            'ỳ' => 'y',
            'ỷ' => 'y',
            'ỹ' => 'y',
            'ỵ' => 'y',
            #---------------------------------Y
            'Ý' => 'y',
            'Ỳ' => 'y',
            'Ỷ' => 'y',
            'Ỹ' => 'y',
            'Ỵ' => 'y',
            #---------------------------------DD
            'Đ' => 'd',
            'đ' => 'd',
            #---------------------------------o
            'ó' => 'o',
            'ò' => 'o',
            'ỏ' => 'o',
            'õ' => 'o',
            'ọ' => 'o',
            'ô' => 'o',
            'ơ' => 'o',
            #---------------------------------O
            'Ó' => 'o',
            'Ò' => 'o',
            'Ỏ' => 'o',
            'Õ' => 'o',
            'Ọ' => 'o',
            'Ô' => 'o',
            'Ơ' => 'o',
            #---------------------------------u
            'ú' => 'u',
            'ù' => 'u',
            'ủ' => 'u',
            'ũ' => 'u',
            'ụ' => 'u',
            'ư' => 'u',
            #---------------------------------U
            'Ú' => 'u',
            'Ù' => 'u',
            'Ủ' => 'u',
            'Ũ' => 'u',
            'Ụ' => 'u',
            'Ư' => 'u',
            
            #---------------------------------a^
            'ấ' => 'a',
            'ầ' => 'a',
            'ẩ' => 'a',
            'ẫ' => 'a',
            'ậ' => 'a',
            #---------------------------------A^
            'Ấ' => 'a',
            'Ầ' => 'a',
            'Ẩ' => 'a',
            'Ẫ' => 'a',
            'Ậ' => 'a',
            #---------------------------------a(
            'ắ' => 'a',
            'ằ' => 'a',
            'ẳ' => 'a',
            'ẵ' => 'a',
            'ặ' => 'a',
            #---------------------------------A(
            'Ắ' => 'a',
            'Ằ' => 'a',
            'Ẳ' => 'a',
            'Ẵ' => 'a',
            'Ặ' => 'a',
            #---------------------------------A
            'Á' => 'a',
            'À' => 'a',
            'Ả' => 'a',
            'Ã' => 'a',
            'Ạ' => 'a',
            'Â' => 'a',
            'Ă' => 'a',
            #---------------------------------a
            'ả' => 'a',
            'ã' => 'a',
            'ạ' => 'a',
            'â' => 'a',
            'ă' => 'a',
            'à' => 'a',
            'á' => 'a'
        );
        $str=strtr(trim($str), $VMAP);
        $str=strtr($str, array(                            
            'm²'=>'m',' -'=>'-','&amp;'=>'-','&'=>'-','+'=>'','*'=>'',':'=>'',
                ' '=>'-',"'"=>'','"'=>'','"'=>'','"'=>'',"'"=>'',
                ':'=>'','.'=>'',','=>'','®'=>'R','©'=>'C','!'=>'','?'=>'','~'=>'','#'=>'','%'=>''
                ,'^'=>'','*'=>'','|'=>'','<'=>'','>'=>'','/'=>'','`'=>'','@'=>'A','$'=>'','a%CC%80'=>'a','%CC%81'=>'a','%'=>''));                   
        $str = preg_replace("/(:|;|\"|\/|\(|\)|\')/", "", strtolower($str));
        $str = preg_replace("/(\s)/", "-", strtolower($str));
        $str = str_replace('--','-',$str);        
        $str=strtr(urlencode($str), array('a%CC%80'=>'a','%CC%81'=>'a','%'=>''));            
        $str = $str !=''?$str:'kitty';
        return strtolower($str);
    }
    public static function convert_alias_validate($str)
    {
        global $VMAP;
        $VMAP = array(
            'ế' => 'e',
            'ề' => 'e',
            'ể' => 'e',
            'ễ' => 'e',
            'ệ' => 'e',
            #---------------------------------E^
            'Ế' => 'e',
            'Ề' => 'e',
            'Ể' => 'e',
            'Ễ' => 'e',
            'Ệ' => 'e',
            #---------------------------------e
            'é' => 'e',
            'è' => 'e',
            'ẻ' => 'e',
            'ẽ' => 'e',
            'ẹ' => 'e',
            'ê' => 'e',
            #---------------------------------E
            'É' => 'e',
            'È' => 'e',
            'Ẻ' => 'e',
            'Ẽ' => 'e',
            'Ẹ' => 'e',
            'Ê' => 'e',
            #---------------------------------i
            'í' => 'i',
            'ì' => 'i',
            'ỉ' => 'i',
            'ĩ' => 'i',
            'ị' => 'i',
            #---------------------------------I
            'Í' => 'i',
            'Ì' => 'i',
            'Ỉ' => 'i',
            'Ĩ' => 'i',
            'Ị' => 'i',
            #---------------------------------o^
            'ố' => 'o',
            'ồ' => 'o',
            'ổ' => 'o',
            'ỗ' => 'o',
            'ộ' => 'o',
            #---------------------------------O^
            'Ố' => 'o',
            'Ồ' => 'o',
            'Ổ' => 'o',
            'Ô' => 'o',
            'Ộ' => 'o',
            #---------------------------------o*
            'ớ' => 'o',
            'ờ' => 'o',
            'ở' => 'o',
            'ỡ' => 'o',
            'ợ' => 'o',
            #---------------------------------O*
            'Ớ' => 'o',
            'Ờ' => 'o',
            'Ở' => 'o',
            'Ỡ' => 'o',
            'Ợ' => 'o',
            #---------------------------------u*
            'ứ' => 'u',
            'ừ' => 'u',
            'ử' => 'u',
            'ữ' => 'u',
            'ự' => 'u',
            #---------------------------------U*
            'Ứ' => 'u',
            'Ừ' => 'u',
            'Ử' => 'u',
            'Ữ' => 'u',
            'Ự' => 'u',
            #---------------------------------y
            'ý' => 'y',
            'ỳ' => 'y',
            'ỷ' => 'y',
            'ỹ' => 'y',
            'ỵ' => 'y',
            #---------------------------------Y
            'Ý' => 'y',
            'Ỳ' => 'y',
            'Ỷ' => 'y',
            'Ỹ' => 'y',
            'Ỵ' => 'y',
            #---------------------------------DD
            'Đ' => 'd',
            'đ' => 'd',
            #---------------------------------o
            'ó' => 'o',
            'ò' => 'o',
            'ỏ' => 'o',
            'õ' => 'o',
            'ọ' => 'o',
            'ô' => 'o',
            'ơ' => 'o',
            #---------------------------------O
            'Ó' => 'o',
            'Ò' => 'o',
            'Ỏ' => 'o',
            'Õ' => 'o',
            'Ọ' => 'o',
            'Ô' => 'o',
            'Ơ' => 'o',
            #---------------------------------u
            'ú' => 'u',
            'ù' => 'u',
            'ủ' => 'u',
            'ũ' => 'u',
            'ụ' => 'u',
            'ư' => 'u',
            #---------------------------------U
            'Ú' => 'u',
            'Ù' => 'u',
            'Ủ' => 'u',
            'Ũ' => 'u',
            'Ụ' => 'u',
            'Ư' => 'u',
            
            #---------------------------------a^
            'ấ' => 'a',
            'ầ' => 'a',
            'ẩ' => 'a',
            'ẫ' => 'a',
            'ậ' => 'a',
            #---------------------------------A^
            'Ấ' => 'a',
            'Ầ' => 'a',
            'Ẩ' => 'a',
            'Ẫ' => 'a',
            'Ậ' => 'a',
            #---------------------------------a(
            'ắ' => 'a',
            'ằ' => 'a',
            'ẳ' => 'a',
            'ẵ' => 'a',
            'ặ' => 'a',
            #---------------------------------A(
            'Ắ' => 'a',
            'Ằ' => 'a',
            'Ẳ' => 'a',
            'Ẵ' => 'a',
            'Ặ' => 'a',
            #---------------------------------A
            'Á' => 'a',
            'À' => 'a',
            'Ả' => 'a',
            'Ã' => 'a',
            'Ạ' => 'a',
            'Â' => 'a',
            'Ă' => 'a',
            #---------------------------------a
            'ả' => 'a',
            'ã' => 'a',
            'ạ' => 'a',
            'â' => 'a',
            'ă' => 'a',
            'à' => 'a',
            'á' => 'a'
        );
        $str=strtr(trim($str), $VMAP);                
        $str = preg_replace("/(:|;|\"|\/|\(|\)|\')/", "", strtolower($str));
        $str = preg_replace("/(\s)/", "-", strtolower($str));
        $str = str_replace('--','-',$str);        
        $str=strtr(urlencode($str), array('a%CC%80'=>'a','%CC%81'=>'a','%'=>''));            
        $str = $str !=''?$str:'kitty';
        return strtolower($str);
    }
    public static function isNameValid($name) { 
        $name = str_replace("-"," ",self::convert_alias_validate($name));
        //$pattern = '/^\w+/'; 
        $pattern = '/^[A-Za-z]+/';
        $pattern = '/^[a-zA-Z][A-Za-z\ \-]+$/';
        if (preg_match($pattern, $name)) {
            return false; 
        } 
        return true; 
    }
    public static function isLenghtValid($str,$limit=100) { 
        if (strlen($str)<$limit) {
            return false; 
        } 
        return true; 
    }
    public static function randomCode($characters=5) 
	{
	   $possible = 'ABCDEFGHIKLMNORSPQYW0123456789';
	   $code = '';
	   $i = 0;
	   while ($i < $characters) {
		   $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
		   $i++;
	   }
	   return $code;
    }
    public static function orderStatus($status) 
	{
      $arr = array(0=>"Mới",1=>"Đã giao",-1=>"Đã hủy",2=>"Đang giao");
      if($status >=-1 && $status<=2){
        return $arr[$status];
      }else{
        return "---";
      }
	  
    }
    public static function status($status) 
	{
      $arr = array(0=>"Đã tắt",1=>"Đang bật");
      if($status >=-1 && $status<=2){
        return $arr[$status];
      }else{
        return "---";
      }
	  
    }
    public static function statusDelivery($status) 
	{
      $arr = array(1=>"Đã thanh toán",-1=>"Chưa thanh toán (hủy)",0=>"Chờ thanh toán");
      if($status >=-1 && $status<=2){
        return $arr[$status];
      }else{
        return "---";
      }
	  
    }
    public static function changeUrlDomain($data) 
	{
      $str = str_replace('src="/uploads/data','src="'.env('APP_DOMAIN')."/uploads/data",$data);
      return $str;
    }
    public static function sendNotification($url="",$data="",$serverKey="") {        
        // $data = array(
        //     "to"=>"/topics/all",
        //     "notification"=> array(
        //       "title"=> "QuickStick",
        //       "body"=> "Là một nhãn hàng tiên phong đưa que thử thai đến gần hơn với các chị em phụ nữ ở Việt Nam, QuickStick vẫn luôn phát huy tối đa vai trò của mình trong việc phát hiện có thai sớm nhất và chính xác nhất cho các chị em.",
        //       "mutable_content"=> true,
        //       "sound"=> "Tri-tone"
        //      )
        // );
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $serverKey
        );   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: '  .  curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    public static function statusPayment($status) 
	{
      $arr = array(0=>"Thanh toán khi giao hàng",1=>"thanh toán chuyển khoản",2=>"Thanh toán IB/VISA");
      if($status >=0 && $status<=2){
        return $arr[$status];
      }else{
        return "---";
      }
	  
    }
    public static function statusPaymentOnepay($status) 
	{
      $arr = array(0=>"",1=>"thanh toan nội địa",2=>"Thanh toán quốc tế");
      if($status >=0 && $status<=2){
        return $arr[$status];
      }else{
        return "---";
      }
	  
    }
    public static function typeBrand($type) 
	{
      $arr = array(0=>"Cá nhân",1=>"Nhà thuốc");
      if($type >=0 && $type<2){
        return $arr[$type];
      }else{
        return "---";
      }	  
    }
    public static function gender($type) 
	{
      $arr = array(0=>"Nam",1=>"Nữ");
      if($type >=0 && $type<2){
        return $arr[$type];
      }else{
        return "---";
      }	  
    }
}
