<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Categorys;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;
use Session;

class ProductController extends Controller
{
    public function list(){        
        $typeUser = Session::get('admin')->type;
        if($typeUser == 0){
            $lists = Products::orderBy('id','desc')->paginate(15);
            return view("admin.product.list",['lists'=>$lists]);
        }
        else if($typeUser == 2){
            return redirect("admin/order/list");
        }
        else if($typeUser == 3){
            return redirect("admin/user/list");
        }
        else{       
            $lists = Products::orderBy('id','desc')->paginate(15);
            return view("admin.product.list",['lists'=>$lists]);
        }
    }
    public function add(){
        $lists = Categorys::orderBy('id','asc')->get();   
        return view('admin.product.add',['list_cate'=>$lists]);
    }
    public function edit(Request $res){
        $data = Products::where("id","=",$res->id)->first();
        $cate = Categorys::orderBy('id','asc')->get();
        return view("admin.product.edit",['data'=>$data,'list_cate'=>$cate]);
    }

    public function editProductAction(Request $res){        
        $cate = Products::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['summary'] = $res->summary;
        $data['price'] = str_replace(",","",$res->price);
        $data['content'] = Helper::changeUrlDomain($res->content);
        $data['category_id'] = $res->category_id==""?1:$res->category_id;
        $data['status'] = $res->status==1?1:0;
        $data['hot'] = $res->hot==1?1:0;
		$data['promotion'] = $res->promotion;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/product/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['images'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $cate->update($data);
        return redirect("admin/product/list");
    }
    //action
    public function addProductAction(Request $res){    
        $data = new Products();
        $data->name = $res->title;
        $data->alias = Helper::convert_alias($res->title);
        $data->summary = $res->summary;
        $data->price = str_replace(",","",$res->price);
        $data->content = Helper::changeUrlDomain($res->content);
        $data->category_id = $res->category_id==""?1:$res->category_id;
        $data->status = $res->status==1?1:0;
        $data->hot = $res->hot==1?1:0;
		$data->promotion = $res->promotion;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/product/add")->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
        }else{
            $image = "";
        }
        $data->images = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        $data->save();
        return redirect("admin/product/list");
    }    
    public function deleteProductAction(Request $res){        
        Products::where("id","=",$res->id)->delete();
        return redirect("admin/product/list");
    }
}
