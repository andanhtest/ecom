<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
    public function edit(Request $res){

        $data = Contact::first();
        return view("admin.contact.edit",['data'=>$data]);
    }

    public function editContactAction(Request $res){        
        $cate = Contact::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->name;
        $data['summary'] = $res->summary;
        $data['slogan'] = $res->slogan;
        $data['note'] = $res->note;
        $data['summary'] = $res->summary;
        $data['url'] = $res->url;
        $data['phone'] = $res->phone;
        $data['fax'] = $res->fax;
        $data['address'] = $res->address;
        $data['email'] = $res->email;

        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/contact/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['image'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $cate->update($data);
        return redirect("admin/contact");
    }
}
