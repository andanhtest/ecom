<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Service\BaseResponse;
use Carbon\Carbon;
use Validator;
use Hash;
use App\Helpers\Fibos;
use App\Helpers\Helper;
use GuzzleHttp\Exception\ClientException;
use App\User;
use App\UserOTP;
use App\Setting;
use App\Banners;
use App\Contact;
use App\UrlWebsite;
use App\Orders;
use App\OrderDetail;
use App\Notification;
use App\NotificationLog;
use App\InfoBank;

class AuthController extends BaseController
{    
	public function username()
    {
        return 'username';
    }
    public function login(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => 'required|string'
        ]);
		
		$user = User::where('phone', $request->get('username'))->first();
		if($user){
			$http = new \GuzzleHttp\Client;			
			try {
				$response = $http->post(env('APP_URL') . '/oauth/token', [
					'form_params' => [
						'grant_type' => env('GRANT_TYPE'),
						'client_id' => env('CLIENT_ID'),
						'client_secret' => env('CLIENT_PASSWORD'),
						'username' => $user->email,
						'password' => $request->password,
						'scope' => '',
					],
				]);	
			} catch (ClientException $e) {
				return BaseResponse::errorResponse(
					'Username or password is incorrect!',
					false,
					200,
					200,
					'Unauthorized'
				);
			}
	
		}else{
			$credentials = request(['username', 'password']);
			if (!Auth::attempt($credentials)){
				return BaseResponse::customResponse(
					'Username or password is incorrect!!',
					[],
					false,
					200,
					401,
					'Username or password is incorrect'
				);
			}
			$user = $request->user();
			$http = new \GuzzleHttp\Client;
			
			try {
				$response = $http->post(env('APP_URL') . '/oauth/token', [
					'form_params' => [
						'grant_type' => env('GRANT_TYPE'),
						'client_id' => env('CLIENT_ID'),
						'client_secret' => env('CLIENT_PASSWORD'),
						'username' => $request->username,
						'password' => $request->password,
						'scope' => '',
					],
				]);
			} catch (ClientException $e) {
				return BaseResponse::errorResponse(
					'Username or password is incorrect!',
					false,
					200,
					200,
					'Unauthorized'
				);
			}
		}
        $res = json_decode((string)$response->getBody(), true);
        $user->avatar = url('storage/' . $user->avatar);
        return BaseResponse::customResponse(
            'Sign in successfully',
            [
                'access_token' => $res["access_token"],
                'refresh_token' => $res["refresh_token"],
                'token_type' => $res["token_type"],
                //'expires_at' => Carbon::parse(
                //    $res["expires_in"]
                //)->toDateTimeString(),
                'profile' => $user,
				'expires_at' => $res["expires_in"]
            ],
            true,
            200,
            200,
            'Success'
        );
    }
    public function refresh(Request $request)
    {
		global $userI;
        $http = new \GuzzleHttp\Client;
        try {
			$response = $http->post(env('APP_URL') . '/oauth/token', [
				'form_params' => [
					'grant_type' => 'refresh_token',
					'refresh_token' => $request->input('refresh_token'),
					'client_id' => env('CLIENT_ID'),
					'client_secret' => env('CLIENT_PASSWORD'),
					'scope' => '',
				],
			]);
			//$user = $this->user($request);
			$res = json_decode((string)$response->getBody(), true);
			$accessToken = $res['access_token'];
			$responseU = $http->request('GET', env('APP_URL') .'/api/user', [
				'headers' => [
					'Accept' => 'application/json',
					'Authorization' => 'Bearer '.$accessToken,
				],
			]);
			//return $userI = $responseU;
			$data = response()->json([
				'data'=>json_decode((string)$responseU->getBody(), true)
			])->setstatusCode(401,"Unauthorized");
			//return $responseU;
			//return $responseU;
			//return $data;
			return BaseResponse::customResponse(
					'Refresh successfully',
					[
						'profile'=>json_decode((string)$responseU->getBody(), true),
						'access_token' => $res["access_token"],
						'refresh_token' => $res['refresh_token'],
						'token_type' => $res["token_type"],
						'expires_at' => Carbon::parse(
							$res["expires_in"]
						)->toDateTimeString(),						
					],
					true,
					200,
					200,
					'Success'
				);
		}
		catch (ClientException $exception) {
			/*$responseBody = json_decode($exception->getResponse()->getBody(true),true);
			return response()->json([
			'data'=>$responseBody
			])->setstatusCode(401,"Unauthorized");			*/
			return BaseResponse::errorResponse(
                'The refresh token is invalid.',
                false,
                200,
                200,
                'Unauthorized'
            );
		}        
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return BaseResponse::customResponse(
            'Successfully logged out',
            [],
            true,
            200,
            200,
            'Success'
        );
    }
    public function user(Request $request)
    {
        $user = $request->user();
        //$user->avatar = url('storage/' . $user->avatar);
        return BaseResponse::customResponse(
            'Successfully get user profile',
            $request->user(),
            true,
            200,
            200,
            'Success'
        );
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'type_brand' => 'required',
            'type_reg' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
       $ckValidateName = Helper::isNameValid($request->name);
       if($ckValidateName) {
            return BaseResponse::errorResponse(
                'Name validation character error',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
       $ckLenghtName = Helper::isLenghtValid($request->name,50);
       if($ckLenghtName) {
            return BaseResponse::errorResponse(
                'Name validation lenght error',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        $emailValidator = Validator::make(
            $request->all(),
            array(
                'email' => 'unique:users,email',
            )
        );  
        if($emailValidator->fails()) {
           return BaseResponse::errorResponse(
                'Email was exist in system',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        $checkPhone = User::where("phone",$request->phone)->count();
        if($checkPhone>0) {
            return BaseResponse::errorResponse(
                 'Phone was exist in system',
                 false,
                 200,
                 200,
                 'Unprocessable Entity'
             );
         }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
		$input['username'] = ($input['email']);
        $user = User::create($input);
        return $this->sendResponse($user, 'User register successfully.');
    }

    public function active_user_by_otp(Request $res)
    {
        if($res->phone && $res->code){
            $lists = User::where("phone",$res->phone)
            ->where("password_otp",$res->code)->count();
            if ($lists==0) {
                return BaseResponse::errorResponse(
                    'account not exist in system',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }else{
                return BaseResponse::errorResponse(
                    'User is actived',
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }            
        }else{
            return BaseResponse::errorResponse(
                'account not exist in system',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }        
    }

    public function testFibo(Request $res){
		$rand = Fibos::generateRandomCode(6);
		$data = new UserOTP();
		$data->phone = $res->phone;
		$data->code = $rand;
		$data->save();
			
        $createdParams = array(
            'serviceType' => '1'
        );
        $res = Fibos::getClientBalance($createdParams);
		$arr = array(200);
		   if(in_array($res,$arr)){
			   return BaseResponse::errorResponse(
                'successfully',
                true,
                200,
                200,
                'Unprocessable Entity'
            );
		   }else{
			   return BaseResponse::errorResponse(
                'false',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
		   }
	   return true;
    }
    public function sendSmsOtp(Request $res){
		$rand = Fibos::generateRandomCode(6);
		$data = new UserOTP();
		$data->phone = $res->phone;
        $data->code = $rand;
        
        //check user exist
        $ck = User::where("phone",$data->phone)->count();
        if($ck>0){
            return BaseResponse::errorResponse(
                'User exist in system',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        $createdParams = array(
            "senderName" => "ThanhKimSon",
            "phoneNumber" => $res->phone,
            "smsMessage" => "Ma OTP dang ky cua quy khach la ".$rand,
            "smsGUID" => 0,
            'serviceType' => 0
       );
       $res = Fibos::sendMaskedSms($createdParams);
	   if($res=="200"){
			$data->save();			
		   return BaseResponse::errorResponse(
                'Send opt successfully',
                true,
                200,
                200,
                'Unprocessable Entity'
            );
	   }else{
		   return BaseResponse::errorResponse(
                'Send opt false'.$res,
                false,
                200,
                200,
                'Unprocessable Entity'
            );
	   }
   }
   public function activeOtpCode(Request $res){
		$rand = Fibos::generateRandomCode(6);
		$data = UserOTP::where("phone",$res->phone)->where("code",$res->code)->where("status",0)->count();
		$update = UserOTP::where("phone",$res->phone)->where("code",$res->code)->where("status",0);
		$value['status'] = 1;        
	   if($data){	
			$update->update($value);	   
		   return BaseResponse::errorResponse(
                'active successfully',
                true,
                200,
                200,
                'Unprocessable Entity'
            );
	   }else{
		   return BaseResponse::errorResponse(
                'active false',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
	   }
   }
   public function sendSmsOtpResetPass(Request $res){
        if($res->get('username')!="" && $res->get('type_reg')!=""){
            if(in_array($res->get('type_reg'),array('phone'))){                
                //phone
                $user = User::where('phone', $res->get('username'))
                            ->orwhere('email', $res->get('username'))
                            ->count();
                if($user>0){
                    $rand = Fibos::generateRandomCode(6);
                    $update = User::where("phone",$res->get('username'));
                    $input['password'] = bcrypt($rand);                    
                    $data = new UserOTP();
                    $data->phone = $res->username;
                    $data->code = $rand;         
                    $createdParams = array(
                        "senderName" => "ThanhKimSon",
                        "phoneNumber" => $res->username,
                        "smsMessage" => "Mat khau moi cua quy khach la ".$rand,
                        "smsGUID" => 0,
                        'serviceType' => 0
                   );
                    $res = Fibos::sendMaskedSms($createdParams);
                    if($res=="200"){
                        $update->update($input);			
                        return BaseResponse::errorResponse(
                                'Send password opt successfully',
                                true,
                                200,
                                200,
                                'Unprocessable Entity'
                            );
                    }else{
                        return BaseResponse::errorResponse(
                                'Send password opt false',
                                false,
                                200,
                                200,
                                'Unprocessable Entity'
                            );
                    }
                }else{
                    return BaseResponse::errorResponse(
                        'Not found',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                }
            }else if(in_array($res->get('type_reg'),array('email'))){
                //email
                $checkEmail = User::where('email', $res->get('username'))->count();
                if($checkEmail>0){
                    return BaseResponse::errorResponse(
                        'Send password new to email successfully',
                        true,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                }else{
                    return BaseResponse::errorResponse(
                        'Send password to email false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                }
                
                
            }else{
                //wrong type
                return BaseResponse::errorResponse(
                    'wrong type',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
            
        }    
    }
    /*
        update profile
    */
    public function update_user(Request $res)
    {
        $validator = Validator::make($res->all(), [
            'name' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'type_brand' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $ckValidateName = Helper::isNameValid($res->name);
        if($ckValidateName) {
             return BaseResponse::errorResponse(
                 'Name validation character error',
                 false,
                 200,
                 200,
                 'Unprocessable Entity'
             );
         }
        $ckLenghtName = Helper::isLenghtValid($res->name,15);
        if($ckLenghtName) {
             return BaseResponse::errorResponse(
                 'Name validation lenght error',
                 false,
                 200,
                 200,
                 'Unprocessable Entity'
             );
         }
        $getUser = $res->user();
        $phone = $getUser->phone;
        $email = $getUser->email;
        $update = User::where("phone",$phone)->where("email",$email);        
        if($update->count()>0) {
            //proress          
            $res->request->remove('phone');
            $res->request->remove('email');
            $input = $res->all();
            $user       = $update->update($input);
            return BaseResponse::errorResponse(
                    'Update profile successfully',
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );            
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        
    }
    public function slider_intro(Request $res){
        $key = md5("thanhkimson");
        if($res->keys && $res->keys === $key){
            $data = Banners::where("type",4)->get();
            return BaseResponse::customResponse(
                'Load setting Successfully',
                $data,
                true,
                200,
                200,
                'Success'
            );
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
    }
    public function setting(Request $res){
        $key = md5("thanhkimson");
        if($res->keys && $res->keys === $key){
            $data = Setting::get();
            $data_web = UrlWebsite::where("group_id",1)->get();
            
            return BaseResponse::customResponse(
                'Load setting Successfully',
               [
                'data_setting'=>json_decode((string)$data, true),
                'data_web'=>json_decode((string)$data_web, true),
               ],
                true,
                200,
                200,
                'Success'
            );
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        
    }
    public function contact(Request $res){
        $key = md5("thanhkimson");
        if($res->keys && $res->keys === $key){
            $data = Contact::get();
            return BaseResponse::customResponse(
                'Load setting Successfully',
                $data,
                true,
                200,
                200,
                'Success'
            );
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        
    }
    public function change_pass(Request $res)
    {
        $validator = Validator::make($res->all(), [
            'password_old' => 'required',
            'password_new' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password_new'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $getUser = $res->user();
        $phone = $getUser->phone;
        $email = $getUser->email;
        $pass = $getUser->password;

        if (!Hash::check($res->password_old, $pass)) {
            return BaseResponse::errorResponse(
                'Password old not right',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        $input['password'] = bcrypt($res->password_new);
        $update = User::where("phone",$phone)->where("email",$email);    
        if($update->count()>0) {
            $user       = $update->update($input);
            return BaseResponse::errorResponse(
                    'Change password successfully',
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );            
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        
    }
    public function checkEmailExist(Request $res){
        $validator = Validator::make($res->all(), [
            'email' => 'required|email'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $email = $res->email;
        $ck = User::where("email",$email)->count();
        if($ck>0){
            return BaseResponse::errorResponse(
                'email already exists',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        return BaseResponse::errorResponse(
            'valid email',
            true,
            200,
            200,
            'Unprocessable Entity'
        ); 
    }
    //order
    public function add_order(Request $res){
        $date_order_limit = date("Y-m-d H:i:s", strtotime("+20 minutes"));
        $rand = Helper::randomCode(6);
        $rand2 = "TKS".Helper::randomCode(10);
        $data = new Orders();
        $data->name = "DH-".$rand."_".$res->info['user_id'];
        $data->user_id = $res->info['user_id'];
        $data->fullname_buyer = $res->info['name'];
        $data->phone_buyer = $res->info['phone'];
        $data->address_buyer = $res->info['address'];
        $data->type = $res->info['type_payment'];
        $data->total = $res->info['total']; 
        $data->location = $res->info['location'];    
        $data->note = $res->info['ship'];
        $data->vat = $res->info['vat'];      
		$data->note = $res->info['note'];      		
        $data->code = $rand2;
        $data->date_order_payment_limit = $date_order_limit;   
        $data->save();
	   if($data){		
           $id = $data->id;			
           //order detail
           $total =0;
            foreach($res->data as $k=>$v){
                $data_od = new OrderDetail();
                $data_od->order_id = $id;
                $data_od->product_id = $v['product_id'];
                $data_od->product_name = $v['product_name'];
                $data_od->amount = $v['amount'];  
                $data_od->price = $v['price'];
                $total += intval($v['amount'])*intval($v['price']);
                $data_od->save();
            }	
           $data = Orders::where("id",$id);
           $value['total'] = $total;
           $value['code'] = $rand2."".$id;     
           $data->update($value); 
           $getData = $data->get();
		   return BaseResponse::customResponse(
                'Save order successfully',
                $getData,
                true,
                200,
                200,
                'Unprocessable Entity'
            );
	   }else{
		   return BaseResponse::errorResponse(
                'Save order false',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
	   }
   }
   public function update_order_status(Request $res){
    if($res->code && $res->user_id && $res->status){
        $update = Orders::where("code",$res->code)
        ->where("user_id",$res->user_id)->where("status",0);
        $value['status'] = $res->status;     
        $update->update($value);   
        if($update){            	   
            return BaseResponse::errorResponse(
                    'Order update successfully',
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );
        }else{
            return BaseResponse::errorResponse(
                    'Order update false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }              
    }else{
        return BaseResponse::errorResponse(
                'Order update false',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
    }
    public function list_order_test(Request $res){
        if($res->user_id){
            $user = $res->user();
            if($user->id==$res->user_id){
                $data = Orders::where("user_id",$res->user_id)->orderBy('id','desc')->get();
                if($data){            	   
                    return BaseResponse::customResponse(
                            'Get order successfully',
                            $data,
                            true,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                }else{
                    return BaseResponse::errorResponse(
                            'Get order false',
                            false,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                    } 
            }else{
                return BaseResponse::errorResponse(
                        'Get order false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                } 
                        
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
	public function list_order(Request $res){
        if($res->user_id){
            $user = $res->user();
            if($user->id==$res->user_id){
                $data = Orders::where("user_id",$res->user_id)->orderBy('id','desc')->get();
                if($data){    
					foreach($data as $k=>$v){
						//DH: cho, 1 da thanh toan, -1 chua thanh toan (status_delivery)
						//GH: moi, 1 da giao, -1 huy						
						if($v['type']==2){
							if($v['status']==1 && $v['status_delivery']==0) {
								//moi 0
								$v['status'] = 0;
							}
							else if($v['status']==1 && $v['status_delivery']==1) {
								//da giao 1
								$v['status'] = 1;
							}
							else if($v['status']==1 && $v['status_delivery']==-1) {
								//huy 1
								$v['status'] = -1;
							}else{
								$v['status']= $v['status'];
							}
						}
						$updated_at =  date("Y-m-d H:i:s", strtotime($v['updated_at']));
						$created_at =  date("Y-m-d H:i:s", strtotime($v['created_at']));
						$data_array[] = array(
									"id"=> $v['id'],
									"name"=> $v['name'],
									"code"=> $v['code'],
									"summary"=> $v['summary'],
									"user_id"=> $v['user_id'],
									"fullname_buyer"=> $v['fullname_buyer'],
									"phone_buyer"=> $v['phone_buyer'],
									"email_buyer"=> $v['email_buyer'],
									"address_buyer"=> $v['address_buyer'],
									"fullname_receiver"=> $v['fullname_receiver'],
									"phone_receiver"=> $v['phone_receiver'],
									"email_receiver"=> $v['email_receiver'],
									"address_receiver"=> $v['address_receiver'],
									"location"=> $v['location'],
									"note"=> $v['note'],
									"total"=> $v['total'],
									"vat"=> $v['vat'],
									"status"=> $v['status'],
									"type"=> $v['type'],
									"date"=> $v['created_at'],
									"created_at"=> $created_at,
									"updated_at"=> $updated_at,
									"status_delivery"=> $v['status_delivery'],
									"type_onepay"=> $v['type_onepay'],
									"date_order_current"=> $v['date_order_current'],
									"date_order_payment_limit" => $v['date_order_payment_limit']
						);
					}
					$result=array();
					$result=$data_array;
                    return BaseResponse::customResponse(
                            'Get order successfully',
                            $result,
                            true,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                }else{
                    return BaseResponse::errorResponse(
                            'Get order false',
                            false,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                    } 
            }else{
                return BaseResponse::errorResponse(
                        'Get order false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                } 
                        
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
	public function get_order_by_id(Request $res){
        if($res->user_id && $res->code){
            $user = $res->user();
            if($user->id==$res->user_id){
                $data = Orders::where("user_id",$res->user_id)
				->where("code",$res->code)
				->get();
                if($data){      	   
                    return BaseResponse::customResponse(
                            'Get order successfully',
                            $data,
                            true,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                }else{
                    return BaseResponse::errorResponse(
                            'Get order false',
                            false,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                    } 
            }else{
                return BaseResponse::errorResponse(
                        'Get order false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                } 
                        
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
    public function get_order_detail_test(Request $res){
        if($res->user_id && $res->code){
            $user = $res->user();
            if($user->id==$res->user_id){
                $data = Orders::where("user_id",$res->user_id)
				->where("code",$res->code)
				->get();
                if($data){ 
                    $orderDetail = OrderDetail::where("order_id",$data[0]['id'])
                    ->get();       	   
                    return BaseResponse::customResponse(
                            'Get order successfully',
                            ["order_info"=>$data,"order_detail"=>$orderDetail],
                            true,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                }else{
                    return BaseResponse::errorResponse(
                            'Get order false',
                            false,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                    } 
            }else{
                return BaseResponse::errorResponse(
                        'Get order false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                } 
                        
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
	public function get_order_detail(Request $res){
        if($res->user_id && $res->code){
            $user = $res->user();
            if($user->id==$res->user_id){
                $data = Orders::where("user_id",$res->user_id)
				->where("code",$res->code)
				->get();
                if($data){foreach($data as $k=>$v){
                    //DH: cho, 1 da thanh toan, -1 chua thanh toan (status_delivery)
                    //GH: moi, 1 da giao, -1 huy						
                    if($v['type']==2){
                        if($v['status']==1 && $v['status_delivery']==0) {
                            //moi 0
                            $v['status'] = 0;
                        }
                        else if($v['status']==1 && $v['status_delivery']==1) {
                            //da giao 1
                            $v['status'] = 1;
                        }
                        else if($v['status']==1 && $v['status_delivery']==-1) {
                            //huy 1
                            $v['status'] = -1;
                        }else{
                            $v['status']= $v['status'];
                        }
                    }
                    $updated_at =  date("Y-m-d H:i:s", strtotime($v['updated_at']));
                    $created_at =  date("Y-m-d H:i:s", strtotime($v['created_at']));
                    $data_array[] = array(
                                "id"=> $v['id'],
                                "name"=> $v['name'],
                                "code"=> $v['code'],
                                "summary"=> $v['summary'],
                                "user_id"=> $v['user_id'],
                                "fullname_buyer"=> $v['fullname_buyer'],
                                "phone_buyer"=> $v['phone_buyer'],
                                "email_buyer"=> $v['email_buyer'],
                                "address_buyer"=> $v['address_buyer'],
                                "fullname_receiver"=> $v['fullname_receiver'],
                                "phone_receiver"=> $v['phone_receiver'],
                                "email_receiver"=> $v['email_receiver'],
                                "address_receiver"=> $v['address_receiver'],
                                "location"=> $v['location'],
                                "note"=> $v['note'],
                                "total"=> $v['total'],
                                "vat"=> $v['vat'],
                                "status"=> $v['status'],
                                "type"=> $v['type'],
                                "date"=> $v['created_at'],
                                "created_at"=> $created_at,
                                "updated_at"=> $updated_at,
                                "status_delivery"=> $v['status_delivery'],
                                "type_onepay"=> $v['type_onepay'],
                                "date_order_current"=> $v['date_order_current'],
                                "date_order_payment_limit" => $v['date_order_payment_limit']
                    );
                }
                $result=array();
                $result=$data_array;
				
                    $orderDetail = OrderDetail::where("order_id",$data[0]['id'])
                    ->get();       	   
                    return BaseResponse::customResponse(
                            'Get order successfully',
                            ["order_info"=>$result,"order_detail"=>$orderDetail],
                            true,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                }else{
                    return BaseResponse::errorResponse(
                            'Get order false',
                            false,
                            200,
                            200,
                            'Unprocessable Entity'
                        );
                    } 
            }else{
                return BaseResponse::errorResponse(
                        'Get order false',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                } 
                        
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
	
    public function list_notification(){
        $data = Notification::where("status",0)->get();
        if($data){            	   
            return BaseResponse::customResponse(
                    'Get notification successfully',
                    $data,
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );
        }else{
            return BaseResponse::errorResponse(
                    'Get order false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
    }
    public function update_notification_by_device(Request $res){
        if($res->device_id && $res->status && $res->notification_id){           
            $update = NotificationLog::where("device_id",$res->device_id);
            $checkExist = Notification::where("id",$res->notification_id)->count();
            $add = new NotificationLog();
            if($checkExist>0){
                if($update->count()>0){
                    $value['status'] = -1;     
                    $update->update($value);   
                    if($update){            	   
                        return BaseResponse::errorResponse(
                                'Notification update successfully',
                                true,
                                200,
                                200,
                                'Unprocessable Entity'
                            );
                    }else{
                        return BaseResponse::errorResponse(
                                'Notification update false',
                                false,
                                200,
                                200,
                                'Unprocessable Entity'
                            );
                    }            
                }else{                    
                        $add->notification_id = $res->notification_id;        
                        $add->status = $res->status;   
                        $add->device_id = $res->device_id;        
                        $add->device = $res->device;        
                        $add->save();
                        return BaseResponse::errorResponse(
                                'Notification update successfully',
                                true,
                                200,
                                200,
                                'Unprocessable Entity'
                            );
                    
                }
                    
            }else{
                return BaseResponse::errorResponse(
                    'Notification update false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
            }
        }else{
            return BaseResponse::errorResponse(
                    'Notification update false',
                    false,
                    200,
                    200,
                    'Unprocessable Entity'
                );
        }
    }
    public function check_notification_device(Request $res){
        if($res->device_id && $res->notification_id){
            $data = NotificationLog::where("device_id",$res->device_id)
            ->where("notification_id",$res->notification_id);
            if($data->count()>0){
                return BaseResponse::errorResponse(
                    'Device has notification',
                    true,
                    200,
                    200,
                    'Unprocessable Entity'
                );       
            }else{
                return BaseResponse::errorResponse(
                        'Device not yet notification',
                        false,
                        200,
                        200,
                        'Unprocessable Entity'
                    );
                }
        }else{
            return BaseResponse::errorResponse(
                'Device not yet notification',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
    }
	
	//
	public function info_bank(Request $res){
        $key = md5("thanhkimson");
        if($res->keys && $res->keys === $key){
            $data = InfoBank::get();
            return BaseResponse::customResponse(
                'Load bank Successfully',
                $data,
                true,
                200,
                200,
                'Success'
            );
        }else{
            return BaseResponse::errorResponse(
                'Not found',
                false,
                200,
                200,
                'Unprocessable Entity'
            );
        }
        
    }
	
	public function getTokenGender(Request $res){
        $key = md5("thanhkimson");
		echo $data = csrf_token();exit;
		return BaseResponse::customResponse(
			'Load token Successfully',
			$data,
			true,
			200,
			200,
			'Success'
		);
        
    }
    


}
