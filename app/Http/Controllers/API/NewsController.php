<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use App\News;
use App\Banners;

class NewsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //    
    public function list()
    {
        $lists = News::orderBy('id','desc')->get();
        $banner = Banners::where("type",1)
                ->orderBy('id','desc')->get();

        if (is_null($lists)) {
            return $this->sendError('Product not found.');
        }
        $arr =array("banner"=>$banner->toArray(),"list_new"=>$lists->toArray());
        return $this->sendResponse($arr, 'Product retrieved successfully.');
    }
    
    public function detail(Request $res)
    {
        $lists = News::where("id",$res->id)->orderBy('id','desc')->get();

        if (is_null($lists)) {
            return $this->sendError('Product not found.');
        }
        return $this->sendResponse($lists->toArray(), 'Product retrieved successfully.');
    }
}
