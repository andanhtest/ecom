<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetail;
use App\Helpers\Helper;
use App\Helpers\OnePay;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
    public function list(){
        $lists = Orders::orderBy('id','desc')->paginate(15);
        return view("admin.order.list",['lists'=>$lists]);
    }
    public function listStatus(Request $res){
        $lists = array();
        if(isset($res->status) && in_array($res->status,array(0,1,-1))){
            $lists = Orders::where("status_delivery",$res->status)
                            ->orderBy('id','desc')->paginate(15);
        }        
        return view("admin.order.list",['lists'=>$lists]);
    }
    public function edit(Request $res){
        $data = Orders::where("id","=",$res->id)->first();
        $order_detail = OrderDetail::select('order_detail.id','order_detail.order_id','order_detail.product_id','order_detail.product_name','order_detail.amount','order_detail.price','products.id','products.promotion')
		->join('products','order_detail.product_id','=','products.id')
		->where("order_id","=",$res->id)->get();
        return view("admin.order.edit",['data'=>$data,'order_detail'=>$order_detail]);
    }

    public function editOrderAction(Request $res){ 
        $ck = Orders::where("id","=",$res->id)->first();       
        $cate = Orders::where("id","=",$res->id);
        $data = array();
        $arr_status = array(1,-1);
        if($ck->status==0){            
            if(in_array($res->status,$arr_status)){
                $data['status'] = $res->status;    
                $cate->update($data);
            }            
        }
        if(isset($ck->status_delivery) && (in_array($res->status,$arr_status) or $ck->status!=0)){
            $arr_delivery = array(1,-1);
            if(in_array($res->status_delivery,$arr_delivery)){
                $data['status_delivery'] = $res->status_delivery;    
                $cate->update($data);
            }            
        }
        return redirect("admin/order/list");
    }
    public function deleteOrderAction(Request $res){        
        Orders::where("id","=",$res->id)->delete();
        return redirect("admin/order/list");
    }
    public function paymentOrder(Request $res){
        $data = array();
        $data['name']	= $res->name;
		$data['city']		= $res->city;
		$data['district']	= $res->district;
		$data['ward']		= $res->ward;
		$data['phone']		= $res->phone;
		$data['email']		= $res->email;
		$data['address']		= $res->address;
		$data['user_id']			= $res->userid;
		$data['total']			= $res->total;		
		$data['hinhthuctt']		= $res->hinhthuctt;
        $data['order_id']		= $res->order_id;
        //print_r($data); exit;
        OnePay::paymentOnepay($data);
       
        //return "return";
    }
    public function complete(){
        //return redirect('not-found');
        $SECURE_SECRET = env("SECURE_SECRET_PAYMENT_BANK");
        $vpc_Txn_Secure_Hash = isset($_GET["vpc_SecureHash"])?$_GET["vpc_SecureHash"]:"";
        if($vpc_Txn_Secure_Hash==="")  return redirect('not-found');        
        unset ( $_GET["vpc_SecureHash"] );
        $errorExists = false;
        ksort ($_GET);
        if (strlen ( $SECURE_SECRET ) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {
            $stringHashData = "";
            foreach ( $_GET as $key => $value ) {
        //        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
        //            $stringHashData .= $value;
        //        }
        //      *****************************chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về*****************************
                if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $stringHashData .= $key . "=" . $value . "&";
                }
            }
        //  *****************************Xóa dấu & thừa cuối chuỗi dữ liệu*****************************
            $stringHashData = rtrim($stringHashData, "&");            
        //    if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper ( md5 ( $stringHashData ) )) {
        //    *****************************Thay hàm tạo chuỗi mã hóa*****************************
            if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*',$SECURE_SECRET)))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "CORRECT";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "INVALID HASH";
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "INVALID HASH";
        }
        // Define Variables
        // ----------------
        // Extract the available receipt fields from the VPC Response
        // If not present then let the value be equal to 'No Value Returned'
        // Standard Receipt Data
        $amount = OnePay::null2unknown ( $_GET["vpc_Amount"] );
        $locale = OnePay::null2unknown ( $_GET["vpc_Locale"] );
        //$batchNo = OnePay::null2unknown ( $_GET["vpc_BatchNo"] );
        $command = OnePay::null2unknown ( $_GET["vpc_Command"] );
        //$message = OnePay::null2unknown ( $_GET["vpc_Message"] );
        $version = OnePay::null2unknown ( $_GET["vpc_Version"] );
        //$cardType = OnePay::null2unknown ( $_GET["vpc_Card"] );
        $orderInfo = OnePay::null2unknown ( $_GET["vpc_OrderInfo"] );
        //$receiptNo = OnePay::null2unknown ( $_GET["vpc_ReceiptNo"] );
        $merchantID = OnePay::null2unknown ( $_GET["vpc_Merchant"] );
        //$authorizeID = OnePay::null2unknown ( $_GET["vpc_AuthorizeId"] );
        $merchTxnRef = OnePay::null2unknown ( $_GET["vpc_MerchTxnRef"] );
        $transactionNo = OnePay::null2unknown ( $_GET["vpc_TransactionNo"] );
        //$acqResponseCode = OnePay::null2unknown ( $_GET["vpc_AcqResponseCode"] );
        $txnResponseCode = OnePay::null2unknown ( $_GET["vpc_TxnResponseCode"] );
       // echo OnePay::getResponseDescription ( $txnResponseCode );
        //echo "<br>";
        $transStatus = "";
        self::update_type_payment_order($orderInfo,1);
        if($hashValidated=="CORRECT" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch thành công";
            self::update_status_order($orderInfo,1);
            return redirect('payment/success');
        }elseif ($hashValidated=="INVALID HASH" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch Pendding";
            self::update_status_order($orderInfo,-1);
            return redirect('payment/false');
        }else {
            $transStatus = "Giao dịch thất bại";
            self::update_status_order($orderInfo,-1);
            return redirect('payment/false');            
        }
        //return $transStatus;
        // $getOrder = Orders::where("code",$orderInfo)->orderBy('id','desc')->first();
        // $status = OnePay::getResponseDescription($txnResponseCode);
        // $data['status'] = $status;
        // $data['transStatus'] = $transStatus;   
        // $data['amount'] = $amount/100;   
        // $data['orderInfo'] = $orderInfo;   
        // $data['fullname'] = $getOrder['fullname_buyer'];
        // $data['email'] = $getOrder['email_buyer'];
        // return view("theme.orders.complete",['data'=>$data]);
    }
    public function complete_visa(){
        //echo $_GET["vpc_TxnResponseCode"]; exit;
        $SECURE_SECRET = env("SECURE_SECRET_PAYMENT_VISA");
        // get and remove the vpc_TxnResponseCode code from the response fields as we
        // do not want to include this field in the hash calculation
        //$vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
        $vpc_Txn_Secure_Hash = isset($_GET["vpc_SecureHash"])?$_GET["vpc_SecureHash"]:"";
        if($vpc_Txn_Secure_Hash==="")  return redirect('not-found');

        $vpc_MerchTxnRef = $_GET["vpc_MerchTxnRef"];
        //$vpc_AcqResponseCode = $_GET["vpc_AcqResponseCode"];
        unset($_GET["vpc_SecureHash"]);
        // set a flag to indicate if hash has been validated
        $errorExists = false;

        if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {

            ksort($_GET);
            //$md5HashData = $SECURE_SECRET;
            //khởi tạo chuỗi mã hóa rỗng
            $md5HashData = "";
            // sort all the incoming vpc response fields and leave out any with no value
            foreach ($_GET as $key => $value) {
        //        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
        //            $md5HashData .= $value;
        //        }
        //      chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về
                if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $md5HashData .= $key . "=" . $value . "&";
                }
            }
        //  Xóa dấu & thừa cuối chuỗi dữ liệu
            $md5HashData = rtrim($md5HashData, "&");

        //    if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper ( md5 ( $md5HashData ) )) {
        //    Thay hàm tạo chuỗi mã hóa
            if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "CORRECT";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "INVALID HASH";
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "INVALID HASH";
        }

        // Define Variables
        // ----------------
        // Extract the available receipt fields from the VPC Response
        // If not present then let the value be equal to 'No Value Returned'

        // Standard Receipt Data
        $amount = OnePay::null2unknown($_GET["vpc_Amount"]);
        $locale = OnePay::null2unknown($_GET["vpc_Locale"]);
        //$batchNo = OnePay::null2unknown($_GET["vpc_BatchNo"]);
        $command = OnePay::null2unknown($_GET["vpc_Command"]);
        $message = OnePay::null2unknown($_GET["vpc_Message"]);
        $version = OnePay::null2unknown($_GET["vpc_Version"]);
       // $cardType = OnePay::null2unknown($_GET["vpc_Card"]);
        $orderInfo = OnePay::null2unknown($_GET["vpc_OrderInfo"]);
        //$receiptNo = OnePay::null2unknown($_GET["vpc_ReceiptNo"]);
        $merchantID = OnePay::null2unknown($_GET["vpc_Merchant"]);
        //$authorizeID = OnePay::null2unknown($_GET["vpc_AuthorizeId"]);
        $merchTxnRef = OnePay::null2unknown($_GET["vpc_MerchTxnRef"]);
        //$transactionNo = OnePay::null2unknown($_GET["vpc_TransactionNo"]);
        //$acqResponseCode = OnePay::null2unknown($_GET["vpc_AcqResponseCode"]);
        $txnResponseCode = OnePay::null2unknown($_GET["vpc_TxnResponseCode"]);
        // 3-D Secure Data
        $verType = array_key_exists("vpc_VerType", $_GET) ? $_GET["vpc_VerType"] : "No Value Returned";
        $verStatus = array_key_exists("vpc_VerStatus", $_GET) ? $_GET["vpc_VerStatus"] : "No Value Returned";
        $token = array_key_exists("vpc_VerToken", $_GET) ? $_GET["vpc_VerToken"] : "No Value Returned";
        $verSecurLevel = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $_GET["vpc_VerSecurityLevel"] : "No Value Returned";
        $enrolled = array_key_exists("vpc_3DSenrolled", $_GET) ? $_GET["vpc_3DSenrolled"] : "No Value Returned";
        $xid = array_key_exists("vpc_3DSXID", $_GET) ? $_GET["vpc_3DSXID"] : "No Value Returned";
        $acqECI = array_key_exists("vpc_3DSECI", $_GET) ? $_GET["vpc_3DSECI"] : "No Value Returned";
        $authStatus = array_key_exists("vpc_3DSstatus", $_GET) ? $_GET["vpc_3DSstatus"] : "No Value Returned";

        // *******************
        // END OF MAIN PROGRAM
        // *******************

        // FINISH TRANSACTION - Process the VPC Response Data
        // =====================================================
        // For the purposes of demonstration, we simply display the Result fields on a
        // web page.

        // Show 'Error' in title if an error condition
        $errorTxt = "";

        // Show this page as an error page if vpc_TxnResponseCode equals '7'
        if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
            $errorTxt = "Error ";
        }

        // This is the display title for 'Receipt' page 
        $title = $_GET["Title"];

        // The URL link for the receipt to do another transaction.
        // Note: This is ONLY used for this example and is not required for 
        // production code. You would hard code your own URL into your application
        // to allow customers to try another transaction.
        //TK//$againLink = URLDecode($_GET["AgainLink"]);


        $transStatus = "";
        self::update_type_payment_order($orderInfo,2);
        if($hashValidated=="CORRECT" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch thành công";
            self::update_status_order($orderInfo,1);
            return redirect('payment/success');
        }elseif ($hashValidated=="INVALID HASH" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch Pendding";
            self::update_status_order($orderInfo,-1);
            return redirect('payment/false');
        }else {
            $transStatus = "Giao dịch thất bại";
            self::update_status_order($orderInfo,-1);
            return redirect('payment/false');            
        }
        $getOrder = Orders::where("code",$orderInfo)->orderBy('id','desc')->first();
        $status = OnePay::getResponseDescription($txnResponseCode);
        $data['status'] = $status;
        $data['transStatus'] = $transStatus;   
        $data['amount'] = $amount/100;   
        $data['orderInfo'] = $orderInfo;   
        $data['fullname'] = $getOrder['fullname_buyer'];
        $data['email'] = $getOrder['email_buyer'];
        return view("theme.orders.complete_visa",['data'=>$data]);  
        //return $transStatus;
    }
    public function order(){
        csrf_token();
        return view("theme.orders.order",['lists'=>[]]);
    }
    public function payment_false(){
        return view("theme.payment.false");
    }
    public function payment_success(){
        return view("theme.payment.success");
    }
    public function not_found(){
        return "fasle data";
    }
    public function update_status_order($code,$status){
        $data = Orders::where("code",$code);
        if($data->count()>0){
            $value['status'] = $status;    
            $data->update($value);
            return true;
        }else{
            return false;
        } 
    }
    public function update_type_payment_order($code,$type){
        $data = Orders::where("code",$code);
        if($data->count()>0){
            $value['type_onepay'] = $type;    
            $data->update($value);
            return true;
        }else{
            return false;
        } 
    }
    public function exportOrder() 
    {
        return Excel::download(new OrdersExport, 'orders.xlsx');
    }
    
}
