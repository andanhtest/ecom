<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsGroup;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;

class NewsGroupController extends Controller
{
    public function index(){
        echo "index";
        exit;
    }
    public function listNewsGroup(){   
        $lists = NewsGroup::orderBy('id','desc')->get();     
        return view("admin.newsgroup.list",['lists'=>$lists]);
    }
    public function addNewsGroup(){        
        return view("admin.newsgroup.add");
    }
    public function editNewsGroup(Request $res){   
        $newsgroup = NewsGroup::where("id","=",$res->id)->first();
        return view("admin.newsgroup.edit",['newsgroup'=>$newsgroup]);
    }
    public function editNewsGroupAction(Request $res){        
        $newsgroup = NewsGroup::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['summary'] = $res->summary;
        $data['content'] = $res->content;
        $data['status'] = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/newsgroup/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
           //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['images'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $newsgroup->update($data);
        return redirect("admin/newsgroup/list");
    }
    //action
    public function addNewsGroupAction(Request $res){    
        $newsgroup = new NewsGroup();
        $newsgroup->name = $res->title;
        $newsgroup->alias = Helper::convert_alias($res->title);
        $newsgroup->summary = $res->summary;
        $newsgroup->content = $res->content;
        $newsgroup->status = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/newsgroup/add")->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
        }else{
            $image = "";
        }
        $newsgroup->images = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        $newsgroup->save();
        return redirect("admin/newsgroup/list");
    }    
    public function deleteNewsGroupAction(Request $res){        
        NewsGroup::where("id","=",$res->id)->delete();
        return redirect("admin/newsgroup/list");
    }
}
