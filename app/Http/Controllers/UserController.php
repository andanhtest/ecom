<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index(){
        echo "ok";
    }
    public function list(){
        $lists = Users::orderBy('id','desc')->paginate(15);
        return view("admin.user.list",['lists'=>$lists]);
    }
    public function edit(Request $res){
        $data = Users::where("id","=",$res->id)->first();
        return view("admin.user.edit",['data'=>$data]);
    }

    public function editUserAction(Request $res){        
        $cate = Users::where("id","=",$res->id);
        $data = array();
        
        $data['name'] = $res->name;
        $data['address'] = $res->address;      
        $cate->update($data);
        return redirect("admin/user/list");
    }  
    public function deleteUserAction(Request $res){        
        Users::where("id","=",$res->id)->delete();
        return redirect("admin/user/list");
    }
    //excel
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
