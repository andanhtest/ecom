<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banners;
use App\BannerGroup;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    public function list(){
        $lists = Banners::orderBy('id','desc')->get();
        return view("admin.banner.list",['lists'=>$lists]);
    }
    public function add(){
        $lists = BannerGroup::orderBy('id','asc')->get();   
        return view('admin.banner.add',['banner_group'=>$lists]);
    }
    public function edit(Request $res){
        $data = Banners::where("id","=",$res->id)->first();
        $lists = BannerGroup::orderBy('id','asc')->get();   
        return view("admin.banner.edit",['data'=>$data,'banner_group'=>$lists]);
    }

    public function editBannerAction(Request $res){        
        $cate = Banners::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['link'] = $res->link;
        $data['summary'] = $res->summary;
        $data['content'] = $res->content;
        $data['type'] = $res->type;
        $data['status'] = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/banner/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['file'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $cate->update($data);
        return redirect("admin/banner/list");
    }
    //action
    public function addBannerAction(Request $res){    
        $data = new Banners();
        $data->name = $res->title;
        $data->link = $res->link;
        $data->summary = $res->summary;
        $data->content = $res->content;
        $data->type = $res->type;
        $data->status = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/banner/add")->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
        }else{
            $image = "";
        }
        $data->file = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        $data->save();
        return redirect("admin/banner/list");
    }    
    public function deleteBannerAction(Request $res){        
        Banners::where("id","=",$res->id)->delete();
        return redirect("admin/banner/list");
    }
}
