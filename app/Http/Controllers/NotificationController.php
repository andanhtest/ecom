<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\NotificationLogSend;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;

class NotificationController extends Controller
{
   
    public function listNotification(){   
        $lists = Notification::orderBy('id','desc')->get();     
        return view("admin.notification.list",['lists'=>$lists]);
    }
    public function logNotification(){   
        $lists = NotificationLogSend::orderBy('id','desc')->paginate(15);     
        return view("admin.notification.log",['lists'=>$lists]);
    }
    public function addNotification(){        
        return view("admin.notification.add");
    }
    public function editNotification(Request $res){   
        $Notification = Notification::where("id","=",$res->id)->first();
        return view("admin.notification.edit",['data'=>$Notification]);
    }
    public function editNotificationAction(Request $res){        
        $Notification = Notification::where("id","=",$res->id);
        $data = array();
        $data['title'] = $res->title;
        $data['summary'] = $res->summary;
        $data['content'] = $res->content;
        $data['status'] = $res->status==1?0:-1;
        $Notification->update($data);
        return redirect("admin/notification/list");
    }
    //action
    public function addNotificationAction(Request $res){    
        $Notification = new Notification();
        $Notification->title = $res->title;
        $Notification->summary = $res->summary;
        $Notification->content = $res->content;
        $Notification->status = $res->status==1?0:-1;       
        $Notification->save();
        return redirect("admin/notification/list");
    }    
    public function deleteNotificationAction(Request $res){        
        Notification::where("id","=",$res->id)->delete();
        return redirect("admin/notification/list");
    }
    //send
    public function sendNotificationAction(Request $res){
        $url = env('APP_URL_NOTIFICATION');
        $serverKey = env('APP_URL_NOTIFICATION_KEY');    
        $getData = Notification::where("id","=",$res->id);
        if($getData->count()>0){
            $getData = $getData->get();
            $data = array();
            foreach($getData as $k=>$v){
                $data = array(
                    "to"=>"/topics/all",
                    "notification"=> array(
                    "title"=> $v['title'],
                    "body"=> $v['summary'],
                    "mutable_content"=> true,
                    "sound"=> "Tri-tone"
                    )
                );
                $log = new NotificationLogSend();
                $log->title = $v['title'];
                $log->summary = $v['summary'];
                $log->status = $v['status'];
                $log->update_by = 1;
                $log->save();
            }
            Helper::sendNotification($url,$data,$serverKey);            
        }else{
            //
        }
        return redirect("admin/notification/list");
    }
}
