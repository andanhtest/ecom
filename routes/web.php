<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('asdasd/', function () {
    return view('welcome');
});
Route::get("/","AdminAuthController@index");

Route::get('database',function(){
    Schema::create('test_table',function($table){
        $table->increments('id');
        $table->string("ten",200);
    });
    echo "ok";
});

Route::get("db/get", function(){
    $data = DB::table("users")->get();
    echo "<pre>";
    print_r($data);
});
Route::get("db/select",function(){
    $data = DB::table("users")->select(["name","email"])->get();
    echo "<pre>";
    print_r($data);
});
Route::get("db/raw",function(){
    $data = DB::table("users")->select(DB::raw("name as hoten, email"))->where("id",1)->get();
    echo "<pre>";
    print_r($data);
});
Route::get("db/orderby",function(){
    $data = DB::table("users")->select(DB::raw("name as hoten, email"))->where("id","!=",1)->orderBy('id','desc')->get();
    echo "<pre>";
    print_r($data);
});
Route::get("db/skip",function(){
    $data = DB::table("users")->select(DB::raw("name as hoten, email"))->where("id","!=",1)->skip(1)->take(3)->orderBy('id','desc')->get();
    echo "<pre>";
    print_r($data);
});
Route::get("db/count",function(){
    $data = DB::table("users")->select(DB::raw("name as hoten, email"))->where("id","!=",1)->take(3)->orderBy('id','desc');
    echo $data->count();
   // echo "<pre>";
   // print_r($data);
});
Route::get("db/update",function(){
    DB::table("users")->where("id",1)->update(['name'=>'abcd']);
    echo "done";
});
Route::get("db/delete",function(){
    DB::table("users")->where("id",1)->delete();
    echo "done";
});
Route::get("db/truncate",function(){
    DB::table("users")->truncate();
    echo "done";
});
Route::get("model/save",function(){
   $user = new App\User();
   $user->name = str_random(3);
   $user->email = str_random(3)."@gmail.com";
   $user->password = bcrypt("123456");
   $user->save();
    echo "done";
});
Route::get("model/delete",function(){
    $user = App\User::find(1);
    $user->delete();
     echo "done";
 });
 Route::get("model/update",function(){
    $user = App\User::find(2);
    $user->name = str_random(5);
    $user->update();
     echo "done";
 });
Route::get("model/query",function(){
    $user = App\User::find(1);
    echo($user->name);
    echo "done";
});
Route::get("model/users2",function(){   
   $user = new App\Users2();
   $user->name = str_random(3);
   $user->email = str_random(3)."@gmail.com";
   $user->password = bcrypt("123456");
   $user->save();
    echo "done";
});

//back office
Route::get("home",function(){
   echo "hello";exit;
});
//admin
Route::get("admin/login","AdminAuthController@index");
Route::post("admin/login","AdminAuthController@login_admin")->name('admin_auth');
Route::get("admin/logout","AdminAuthController@logout");
Route::get("admin/checklogin","AdminAuthController@checkLogin");
Route::get('refresh-csrf', function(){
    return csrf_token();
});
Route::get("check-helper","AdminAuthController@checkHelper");

//test session
Route::get("session",'AdminAuthController@getSession');
//order
//Route::get("order",'OrderController@order');

Route::group(['prefix'=>'admin', 'middleware'=>'adminLogin'],function(){   
    Route::get("/",'ProductController@list');
    Route::get("dashboard",'ProductController@list');
    
    Route::group(["prefix"=>"news"],function(){
        Route::get("list",'NewsController@listNews');
        Route::get("add",'NewsController@addNews');
        Route::get("edit/{id}",'NewsController@editNews');
        Route::get("delete/{id}",'NewsController@deleteNewsAction')->name('deleteNews');
        //post
        Route::post("add",'NewsController@addNewsAction')->name('addNews');
        Route::post("edit",'NewsController@editNewsAction')->name('editNews');
    });
    Route::group(["prefix"=>"product"],function(){
        Route::get("list",'ProductController@list');
        Route::get("add",'ProductController@add');
        Route::get("edit/{id}",'ProductController@edit');
        Route::get("delete/{id}",'ProductController@deleteProductAction')->name('deleteProduct');
        //post
        Route::post("add",'ProductController@addProductAction')->name('addProduct');
        Route::post("edit",'ProductController@editProductAction')->name('editProduct');
    });
    Route::group(["prefix"=>"category"],function(){
        Route::get("list",'CategoryController@list');
        Route::get("add",'CategoryController@add');
        Route::get("edit/{id}",'CategoryController@edit');
        Route::get("delete/{id}",'CategoryController@deleteCategoryAction')->name('deleteCategory');
        //post
        Route::post("add",'CategoryController@addCategoryAction')->name('addCategory');
        Route::post("edit",'CategoryController@editCategoryAction')->name('editCategory');
    });
    Route::group(["prefix"=>"banner"],function(){
        Route::get("list",'BannerController@list');
        Route::get("add",'BannerController@add');
        Route::get("edit/{id}",'BannerController@edit');
        Route::get("delete/{id}",'BannerController@deleteBannerAction')->name('deleteBanner');
        //post
        Route::post("add",'BannerController@addBannerAction')->name('addBanner');
        Route::post("edit",'BannerController@editBannerAction')->name('editBanner');
    });
    Route::group(["prefix"=>"banner-group"],function(){
        Route::get("list",'BannerGroupController@list');
        Route::get("add",'BannerGroupController@add');
        Route::get("edit/{id}",'BannerGroupController@edit');
        Route::get("delete/{id}",'BannerGroupController@deleteBannerGroupAction')->name('deleteBannerGroup');
        //post
        Route::post("add",'BannerGroupController@addBannerGroupAction')->name('addBannerGroup');
        Route::post("edit",'BannerGroupController@editBannerGroupAction')->name('editBannerGroup');
    });
    Route::group(["prefix"=>"contact"],function(){
        Route::get("list",'ContactController@list');
        Route::get("add",'ContactController@add');
        Route::get("edit",'ContactController@edit');
    });
    Route::group(["prefix"=>"newsgroup"],function(){
        Route::get("list",'NewsGroupController@listNewsGroup');
        Route::get("add",'NewsGroupController@addNewsGroup');
        Route::get("edit/{id}",'NewsGroupController@editNewsGroup');
        Route::get("delete/{id}",'NewsGroupController@deleteNewsGroupAction')->name('deleteNewsGroup');
        //post
        Route::post("add",'NewsGroupController@addNewsGroupAction')->name('addNewsGroup');
        Route::post("edit",'NewsGroupController@editNewsGroupAction')->name('editNewsGroup');        
    });
    Route::group(["prefix"=>"order"],function(){
        Route::get("list",'OrderController@list');
        Route::get("edit/{id}",'OrderController@edit');
        Route::get("delete/{id}",'OrderController@deleteOrderAction')->name('deleteOrder');
        Route::get("list-status/{status}",'OrderController@listStatus');
        //post
        Route::post("edit",'OrderController@editOrderAction')->name('editOrder');        
    });
    Route::group(["prefix"=>"user"],function(){
        Route::get("list",'UserController@list');
        Route::get("edit/{id}",'UserController@edit');
        Route::get("delete/{id}",'UserController@deleteUserAction')->name('deleteUser');
        //post
        Route::post("edit",'UserController@editUserAction')->name('editUser');        
    });
    Route::group(["prefix"=>"account"],function(){
        Route::get("change-password",'AdminAuthController@changePassword');
        //post
        Route::post("change-password",'AdminAuthController@changePasswordAction')->name('change_pass_admin');        
    });
    //setting
    Route::group(["prefix"=>"setting"],function(){
        Route::get("/",'SettingController@edit');
        //post
        Route::post("edit",'SettingController@editSettingAction')->name('editSetting');
    });
    Route::group(["prefix"=>"contact"],function(){
        Route::get("/",'ContactController@edit');
        //post
        Route::post("edit",'ContactController@editContactAction')->name('editContact');
    });
    Route::group(["prefix"=>"url_website"],function(){
        Route::get("list",'UrlWebsiteController@listUrl');
        Route::get("add",'UrlWebsiteController@addUrl');
        Route::get("edit/{id}",'UrlWebsiteController@editUrl');
        Route::get("delete/{id}",'UrlWebsiteController@deleteUrlAction')->name('deleteUrl');
        //post
        Route::post("add",'UrlWebsiteController@addUrlAction')->name('addUrl');
        Route::post("edit",'UrlWebsiteController@editUrlAction')->name('editUrl');        
    });
    //notification
    Route::group(["prefix"=>"notification"],function(){
        Route::get("list",'NotificationController@listNotification');
        Route::get("log",'NotificationController@logNotification');
        Route::get("add",'NotificationController@addNotification');
        Route::get("edit/{id}",'NotificationController@editNotification');
        Route::get("delete/{id}",'NotificationController@deleteNotificationAction')->name('deleteNotification');
        //post
        Route::post("add",'NotificationController@addNotificationAction')->name('addNotification');
        Route::post("edit",'NotificationController@editNotificationAction')->name('editNotification');
        //send
        Route::get("send/{id}",'NotificationController@sendNotificationAction')->name('sendNotification');        
    });
});
Route::get('lienket-newsgroup',function(){
    $data = App\News::find(3)->news_group->toArray();
   // echo "<pre>";
    //print_r($data);
});
Route::get('lienket-news',function(){
    $data = App\NewsGroup::find(1)->news->toArray();
    //echo "<pre>";
    //print_r($data);
});
//payment
Route::group(["prefix"=>"payment"],function(){
    Route::get("complete",'OrderController@complete');
    Route::get("complete_visa",'OrderController@complete_visa');
    //
    Route::get("false",'OrderController@payment_false');
    Route::get("success",'OrderController@payment_success');
});
Route::get("not-found",'OrderController@not_found');
Route::get("maintain-order",'MaintainController@listOrderStatusMaintain');
//Route::get("sendmail",'SendMailController@sendMail');
//excel
Route::get('export', 'UserController@export')->name('export');
Route::get('exportOrder', 'OrderController@exportOrder')->name('exportOrder');
Route::get('importExportView', 'UserController@importExportView');
Route::post('import', 'UserController@import')->name('import');
