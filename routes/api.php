<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get("userApi/",function(){   
//      echo "done";
//  });
Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');
Route::post('login/refresh', 'API\AuthController@refresh');
  
Route::middleware('auth:api')->group( function () {
    Route::group(['prefix'=>'user'],function(){ 
        Route::get('logout', 'API\AuthController@logout');
        Route::post('active', 'API\AuthController@active_user_by_otp');
        Route::post('update', 'API\AuthController@update_user');
        Route::post('change_pass', 'API\AuthController@change_pass');
    });
    Route::group(['prefix'=>'order'],function(){
        Route::post('save', 'API\AuthController@add_order');
        Route::post('update_status', 'API\AuthController@update_order_status');
        Route::post('list', 'API\AuthController@list_order');
		Route::post('list-test', 'API\AuthController@list_order_test');
        Route::post('get_order_by_id', 'API\AuthController@get_order_by_id');
        Route::post('get_order_detail', 'API\AuthController@get_order_detail');
		Route::post('get_order_detail_test', 'API\AuthController@get_order_detail_test');
    });
    //Route::resource('products', "API\ProductController");    
});
Route::group(['prefix'=>'products'],function(){ 
    Route::get('list', "API\ProductController@list");
    Route::get('detail/{id}', "API\ProductController@detail");
});
Route::group(['prefix'=>'news'],function(){ 
    Route::get('list', "API\NewsController@list");
    Route::get('detail/{id}', "API\NewsController@detail");
});
Route::get('test',['middleware' => 'cors' , function(){ 
    echo "ok";
}]);
Route::post('testFibo', 'API\AuthController@testFibo');
Route::post('send_sms_otp', 'API\AuthController@sendSmsOtp');
Route::post('active_code', 'API\AuthController@activeOtpCode');
Route::post('check_email', 'API\AuthController@checkEmailExist');

//reset pass
Route::post('send_sms_otp_setpass', 'API\AuthController@sendSmsOtpResetPass');

//setting
Route::get('slider_intro/{keys}', 'API\AuthController@slider_intro');
Route::get('setting/{keys}', 'API\AuthController@setting');
Route::get('contact/{keys}', 'API\AuthController@contact');
Route::get('info_bank/{keys}', 'API\AuthController@info_bank');
Route::get('get_token', 'API\AuthController@getTokenGender');

//notification
Route::group(['prefix'=>'notification'],function(){
    Route::post('update_device_status', 'API\AuthController@update_notification_by_device');
    Route::post('list', 'API\AuthController@list_notification');
    Route::post('check', 'API\AuthController@check_notification_device');
});

Route::group(["prefix"=>"payment"],function(){
    Route::post("order_data",'OrderController@paymentOrder')->name("paymentOrder");
    Route::get("complete",'OrderController@complete');
    Route::get("complete_visa",'OrderController@complete_visa');
});