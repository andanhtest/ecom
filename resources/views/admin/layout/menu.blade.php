  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="admin_asset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Session::get('admin')->email}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu chính</li>
        @php if(Session::get('admin')->type == 0 || Session::get('admin')->type == 1){   @endphp                   
           
        <li class="treeview @if($menu_active=='newsgroup') active @endif ">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Quản lý loại tin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/newsgroup/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/newsgroup/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
          </ul>
        </li>  
        
        <li class="treeview @if($menu_active=='news') active @endif">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Quản lý tin tức</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/news/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/news/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
          </ul>
        </li>  
        
        <!-- <li class="treeview @if($menu_active=='category') active @endif">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Danh mục sản phẩm</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/category/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/category/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
          </ul>
        </li> -->

        <li class="treeview @if($menu_active=='product') active @endif">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý sản phẩm</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/product/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/product/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
          </ul>
        </li>
        <li class="treeview @if($menu_active=='banner') active @endif">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Quản lý Banner</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/banner/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/banner/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
          </ul>
        </li>
        @php } @endphp

        @php if(Session::get('admin')->type == 3 || Session::get('admin')->type == 1){   @endphp
        <li class="treeview @if($menu_active=='user') active @endif">
          <a href="#">
            <i class="fa fa-table"></i> <span>Quản lý người dùng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/user/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
          </ul>
        </li>
        @php } @endphp

        @php if(Session::get('admin')->type == 2 || Session::get('admin')->type == 1){   @endphp
        <li class="treeview @if($menu_active=='order') active @endif">
          <a href="#">
            <i class="fa fa-book"></i> <span>Quản lý đơn hàng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/order/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/order/list-status/0"><i class="fa fa-circle-o"></i> Mới</a></li>
            <li><a href="admin/order/list-status/1"><i class="fa fa-circle-o"></i> Đã duyệt</a></li>
            <li><a href="admin/order/list-status/-1"><i class="fa fa-circle-o"></i> Đã hủy</a></li>
          </ul>
        </li>
        @php } @endphp

       @php if(Session::get('admin')->type == 1){   @endphp 
        <li class="treeview @if($menu_active=='contact') active @endif">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Contact config</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/contact"><i class="fa fa-circle-o"></i> contact</a></li>
          </ul>
        </li>


        <li class="treeview @if($menu_active=='setting') active @endif">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/setting"><i class="fa fa-circle-o"></i> setting</a></li>
          </ul>
        </li>

        <li class="treeview @if($menu_active=='url_website') active @endif">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Config URLWebsite cho APi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
            <ul class="treeview-menu">
              <li><a href="admin/url_website/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
              <li><a href="admin/url_website/add"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
            </ul>          
        </li>

        <li class="treeview @if($menu_active=='notification') active @endif ">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Quản lý Notification</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin/notification/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="admin/notification/log"><i class="fa fa-circle-o"></i> Danh sách log</a></li>
          </ul>
        </li>  
        @php } @endphp
        
               
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>