@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editUser')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">                
                <div class="form-group">
                  <label for="title">Họ tên</label>
                  <input required type="text" name="name" value="{{$data->name}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Email</label>
                  <input readonly type="text" name="" value="{{$data->email}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Điện thoại</label>
                  <input readonly type="text" name="" value="{{$data->phone}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
				<div class="form-group">
                  <label for="title">Giới tính</label>
                  <input readonly type="text" name="" value="{{Helper::gender($data->gender)}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
				<div class="form-group">
                  <label for="title">Ngày sinh</label>
                  <input readonly type="text" name="" value="{{$data->birthday}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
				<div class="form-group">
                  <label for="title">Loại đăng ký</label>
                  <input readonly type="text" name="" value="{{Helper::typeBrand($data->type_brand)}}" class="form-control" id="title" placeholder="">
                </div>
                <div class="form-group">
                  <label for="title">Địa chỉ</label>
                  <input type="text" name="address" value="{{$data->address}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
