@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              
              
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if (session('message'))
              <div class="alert alert-danger ">
                  {{ session('message') }}
              </div>
            @endif
            <form role="form" action="{{route('change_pass_admin')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">                
                <div class="form-group">
                  <label for="title">Họ tên</label>
                  <input readonly type="text" value="{{$data->name}}" class="form-control" id="title" placeholder="Họ tên">
                </div>
                <div class="form-group">
                  <label for="title">Mật khẩu cũ</label>
                  <input required minlength="6" type="password" name="passold" value="" class="form-control" id="title" placeholder="Mật khẩu cũ">
                </div>
                <div class="form-group">
                  <label for="title">Mật khẩu mới</label>
                  <input required minlength="6" type="password" name="passnew" value="" class="form-control" id="title" placeholder="Mật khẩu mới">
                </div>        
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
