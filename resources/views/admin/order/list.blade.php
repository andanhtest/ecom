@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Danh sách
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Danh sách</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh sách </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Stt</th>
                  <th>Đơn hàng</th>
                  <th>Ngày</th>                  
                  <th>Tổng tiền</th>
                  <th>Giao hàng</th>
                  <th>Trạng thái TT</th>
                  <th>Phương thức</th>
                  <th>Chức năng</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lists as $k=>$v)
                @php
                    if($v->type == 2){
                      if($v->status == 1 && $v->status_delivery == 0) {
                        $v->status = 0;
                      }
                      else if($v->status == 1 && $v->status_delivery == 1) {
                        $v->status = 1;
                      }
                      else if($v->status == 1 && $v->status_delivery == -1) {
                        $v->status = -1;
                      }else{
                        $v->status = $v->status;
                      }
                    }
                @endphp
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->code}}</td>
                  <td>{{$v->created_at}}</td>
                  <td>{{$v->total}}</td>
                  <td>{{Helper::orderStatus($v->status)}}</td>
                  <td>{{Helper::statusDelivery($v->status_delivery)}}</td>
                  <td>{{Helper::statusPayment($v->type)}}</td>
                  <td><a onclick="return confirm('Bạn muốn xóa?');" href="admin/order/delete/{{$v->id}}">Xóa | 
                  <a href="admin/order/edit/{{$v->id}}">Cập nhật</td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
            @if(count($lists)>0)
              {{ $lists->links() }}
            @endif
          </div>
          <!-- /.box -->

          <a class="btn btn-warning" href="{{ route('exportOrder') }}">Export Data</a>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection