@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editOrder')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">               
                 
                <div class="form-group">
                  <label for="title">Đơn hàng</label>
                  <input readonly type="text" name="title" value="{{$data->name}}" class="form-control" id="title" placeholder="">
                </div>
                <div class="form-group">
                  <label for="title">Mã DH</label>
                  <input readonly type="text" name="title" value="{{$data->code}}" class="form-control" id="title" placeholder="">
                </div>
                <div class="form-group">
                  <label for="title">Tổng giá</label>
                  <input readonly type="text" name="title" value="{{$data->total}}" class="form-control" id="title" placeholder="">
                </div>
                <hr>
                <div class="col-md-12">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="title">Họ tên người mua</label>
                    <input readonly type="text" name="title" value="{{$data->fullname_buyer}}" class="form-control" id="title" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="title">Điện thoại người mua</label>
                    <input readonly type="text" name="title" value="{{$data->phone_buyer}}" class="form-control" id="title" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="title">Email người mua</label>
                    <input readonly type="text" name="title" value="{{$data->email_buyer}}" class="form-control" id="title" placeholder="">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="title">Địa chỉ người mua</label>
                    <input readonly type="text" name="title" value="{{$data->address_buyer}}" class="form-control" id="title" placeholder="">
                  </div>   
                  <div class="form-group">
                    <label for="title">Thuộc vị trí</label>
                    <input readonly type="text" name="title" value="{{$data->location}}" class="form-control" id="title" placeholder="">
                  </div>
                  <div class="form-group">
                      <label for="title">Ghi chú</label>
                      <input readonly type="text" name="title" value="{{$data->note}}" class="form-control" id="title" placeholder="">
                  </div>  
                  </div>               
                </div>
                
                <!-- <div class="checkbox">
                  <label>
                    <input type="checkbox" name="status" value="1" @if($data->status==1) checked @endif > Hiển thị
                  </label>
                </div> -->
                

              </div>
              <!-- /.box-body -->

              <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Stt</th>
                  <th>Sản phẩm</th>
                  <th>Số lượng</th>
				  <th>Khuyến mãi</th>
                  <th>Giá</th>
                </tr>
                </thead>
                <tbody>
                @if(count($order_detail)>0)
                @foreach($order_detail as $k=>$v)
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->product_name}}</td>
                  <td>{{$v->amount}}</td>
				  <td>{{$v->promotion}}</td>
                  <td>{{$v->price}}</td>
                </tr>
                @endforeach
                <tr>
                  <td>Tổng tiền</td>
                  <td></td>
                  <td></td>
                  <td>{{$data->total}}</td>
                </tr>
                @endif
                </tbody>
               
              </table>
              
            </div>

              <div class="box-footer">
              <div class="clearfix visible-xs-block"></div>
              @if($data->status==0)
                <div class="form-group col-md-3">
                    <label>Cập nhật trạng thái</label>
                     <select class="form-control " name="status">
                      <option value="0">Mới</option>
                      <option value="1">Đã giao</option>
                      <option value="-1">Đã hủy</option>
                    </select>
                </div>
                @else
                  <div class="form-group col-md-3">
                    <label>Trạng thái: </label>
                  {{Helper::orderStatus($data->status)}}
                  </div>
                @endif
                <div class="clearfix visible-xs-block"></div>
                @if($data->status_delivery==0)
                <div class="form-group col-md-3">
                    <label>Thanh toán</label>
                     <select class="form-control " name="status_delivery">
                      <option value="">Chờ thanh toán</option>
                      <option value="1">Đã thanh toán</option>
                      <option value="-1">Chưa thanh toán</option>
                    </select>
                </div>
                @else
                  <div class="form-group col-md-3">
                    <label>Thanh toán: </label>
                  {{Helper::statusDelivery($data->status_delivery)}}
                  </div>
                @endif

                <div class="clearfix visible-xs-block"></div>
              <div class="col-md-12">
              @if($data->status==0)
                <button type="submit" class="btn btn-primary">Cập nhật</button>
                @else
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              @endif 
                </div>  
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
