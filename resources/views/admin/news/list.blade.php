@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Danh sách
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Danh sách</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh sách <a href="admin/news/add">[thêm mới]</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Stt</th>
                  <th>Tiêu đề</th>
                  <th>Nhóm tin</th>
                  <th>Ngày</th>
                  <th>Trạng thái</th>
                  <th>Chức năng</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lists as $k=>$v)
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->name}}</td>
                  <td>@if($v->news_group_id>0){{$v->news_group->name}}@endif</td>
                  <td>{{$v->created_at}}</td>
                  <td>{{Helper::status($v->status)}}</td>
                  <td><a onclick="return confirm('Bạn muốn xóa?');" href="admin/news/delete/{{$v->id}}">Xóa | 
                  <a href="admin/news/edit/{{$v->id}}">Cập nhật</td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
            @if(count($lists)>0)
              {{ $lists->links() }}
            @endif
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection