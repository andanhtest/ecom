@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($newsgroup))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editNewsGroup')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$newsgroup->id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Tiêu đề</label>
                  <input required type="text" name="title" value="{{$newsgroup->name}}" class="form-control" id="title" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="summary">Mô tả</label>
                  <textarea name="summary" class="form-control" id="" placeholder="Mô tả">{{$newsgroup->summary}}</textarea>
                </div>
                <div class="form-group">
                  @if($newsgroup->images !="")
                  <img src="{{$newsgroup->images}}" style="width:100px; height:100%;"/>
                  @endif
                  <br>
                  <label for="exampleInputFile">Cập nhật hình</label>
                  <input type="file" id="image" name="image">
                  <p class="help-block">chỉ dùng [.png .jpg .gif]</p>
                </div>

               
                <!-- textarea -->
                <div class="form-group">
                  <label>Nội dung</label>
                  <textarea class="form-control" name="content" id="editor1" rows="3" placeholder="Enter ...">{{$newsgroup->content}}</textarea>
                </div>     

                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="status" value="1" @if($newsgroup->status==1) checked @endif > Hiển thị
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
