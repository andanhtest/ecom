<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(DBSeed::class);

    }
}
class DBSeed extends Seeder{
    function run(){        
        DB::table("admins")->insert([
            ['name'=>"van a","email"=>str_random(3)."@gmail.com","password"=>bcrypt('123456')],
            ['name'=>"van b","email"=>str_random(3)."@gmail.com","password"=>bcrypt('123456')],
            ['name'=>"van c","email"=>str_random(3)."@gmail.com","password"=>bcrypt('123456')]
        ]);
        
    }
}
